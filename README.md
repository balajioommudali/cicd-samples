### Title
This project provides samples for different CI/CD scenarios, @Samer.akkoub created and used it mainly to demo GitLab key CI/CD capabilities. 

### Included Scenarios 
1. Building and exporting artifacts. 
2. Creating dynamic Jobs 
3. Building, scanning and deploying NodeJS project, note you need to define/update the following variables in the .deploy-NodeJs-App.yml file 
   - KUBE_CONTEXT: "sakkoub-group/cicd-workshop/gke-sandbox-cluster:kas-agent-config-sandbox"
   - KUBE_INGRESS_BASE_DOMAIN: "34.151.64.31.nip.io"
   - KUBE_NAMESPACE: "my-ns"
4. Infrastructure Scanning 
5. Defining and inheriting environment variables. 
6. Jobs extension 
7. Jobs grouping 
8. Jobs referencing (in the .labs-ci.yml file )
9. Parent child, multi-project and metrix-job samples
10. Accessability testing
11. Performance testing  
12. Postman testing 
13. Vault integration 
14. Requirments confirmation (deprecated feature)
